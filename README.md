# political-commercial-collection-archives


The [Carl Albert Center Archives](https://www.ou.edu/carlalbertcenter/congressional-collection), along with Harvard University and the University of Iowa, was awarded collaborative research grant funds from the National Science Foundation (NSF) for the project ["Understanding the Evolution of Political Campaign Advertisements over the Last Century"](https://s-lib024.lib.uiowa.edu/campaignvids/people.html).

The project has three specific aims: (1) make a large underutilized collection of  over 120,000 political ads (1912-2016) currently housed at the Carl Albert Center suitable for academic and non-academic research; (2) understand the evolution of political advertising, especially as it pertains to issue advocacy and gender/minority representations prior to 1996; and, (3) promote interdisciplinary graduate and undergraduate education in audiovisual analysis at research and teaching institutions. ​

The Carl Albert Center Archives' task focused on the first aim (1) by providing reliable content for collaborator analysis. The project has provided invaluable insights and knowledge, despite the initial challenges encountered. This page was created to document our processes and share solutions which may benefit other collection entities. 

# table of contents

[**key research components**](key-research-components)

[**description of case study**](description-of-case-study)

>> [Julian P. Kanter Archive](#Julian-P-Kanter-Archive)

>> [collection growth and mismanagement issues](#collection-growth-and-mismanagement-issues)

>> [technological shifts and the increase in collection materials](#technological-shifts-and-increase-in-collection-materials)

>> [issues with system infrastructure and normalization](#issues-with-system-infrastructure-and-normalization)

>> [2019 data](#2019-data)


[**entropy and archival dis-order**](entropy-and-archival-disorder)

>> [identification of human-centered patterns](#identification-of-human-centered-patterns)

>> [backlogs as a critical point of failure](#backlogs-as-a-critical-point-of-failure)

[**control methods**](control-methods)

>> [data cleansing](#data-cleansing)

>> [data aggregation](#data-aggregation)

>> [standardization](#standardization)

>> [link data](#link-data)

[**appendix 1  archival metadata**](appendix-1)

>> [1985](#appendix-1-archival-metadata-1985)

>> [1996](#appendix-1-archival-metadata-1996)

>> [2003](#appendix-1-archival-metadata-2003)

>> [2019](#appendix-1-archival-metadata-2019)

>> [current](#appendix-1-archival-metdata-current)

[**appendix 2 data accuracy index**](appendix-2)

[**appendix 3 additional resources**](appendix-3)

>> [general references](#general-references)

>> [unresolved issues further questions](#unresolved-issues-further-questions)



#

# [**key research components**](key-research-components)

1. Address the challenges of insufficient documentation, undefined workflows, and limited funding in managing the growing collection of political ads.
2. Develop workflows to effectively manage the existing collection of political ads.
3. Define strategies to ensure the long-term accessibility and usability of its digital political ad collection.
4. Explore and define practical solutions and best practices in managing and preserving large archival collections in academic settings.

